#importing libriaries
from influxdb import InfluxDBClient
import random
import time
client = InfluxDBClient(host='54.161.55.1', port=8086, username='admin', password='admin', ssl=True, verify_ssl=False)
print(client)
print(client.get_list_database())
client.switch_database('smartgrid')

#list of data that will be imported to the databse
data = []
i = 0
while(i<=8000000):
    data.append("measure,id={id},current={current},voltage={voltage} sectorId={sectorId}"
            .format(
                    id=random.randrange(1000),
                    current=random.uniform(1,1.5),
                    voltage = random.randint(150,220),
                    sectorId=random.randrange(1000)
                    ))
    time.sleep(5000)
    if(i%10000):
        print(i+" over 8000000")
client.write_points(data, database='smartgrid', time_precision='ms', batch_size=10000, protocol='line')