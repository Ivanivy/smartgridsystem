import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { PlusMath } from '../../images/index';
import Device from './Device/Device';
import './Devices.css';


class Devices extends Component {
    render() {
        return (
            <div className="page-contain devices">
                <Device name="fridge" history={this.props.history} />
                <Device name="lamp" history={this.props.history}/>
                <Device name="tv" history={this.props.history}/>
                <Device name="fridge" label="fridge 2" history={this.props.history}/>
                <Device name="lamp" label="room frid" history={this.props.history}/>
                <Device name="tv" label="room tv" history={this.props.history}/>
                <div className="fab-button">
                    <PlusMath />
                </div>
            </div>
        )
    }
}
export default withRouter(Devices);