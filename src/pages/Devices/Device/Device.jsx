import React from 'react';
import './Device.css';
import { Fridge, Tv, Lamp } from '../../../images/index';
import { routes } from '../../../utils/index';
export default function Device({ name = "fridge", label = "", history }) {

    const options = {
        fridge: { image: <Fridge />, label: "fridge" },
        tv: { image: <Tv />, label: "tv" },
        lamp: { image: <Lamp />, label: "lamp" },
    }

    return (
        <div className="card-item device-item" onClick={() => { history.push(routes.mainpage + routes.devicedetails); }}>
            <div className="device-image">{options[name].image}</div>
            <div className="device-label"> {label != "" ? label : options[name].label}</div>
        </div>
    )
}
