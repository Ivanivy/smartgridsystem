import React, { Component } from 'react';
import './Control.css';
import { ToogleButton, Button, InputField } from '../../components/index';
export default class Control extends Component {
    state = {
        open: false
    }
    render() {
        return (
            <div className="page-contain control">
                <div className="card-item">
                    <div className="control-item">
                        <div>
                            Activer le plafond:
                        </div>
                        <div>
                            <ToogleButton />
                        </div>
                    </div>
                    <div className="control-item">
                        <div>
                            Prix:
                        </div>
                        <div>
                            10 000 FCFA
                        </div>
                    </div>
                    <div className="control-item">
                        <div>
                            Cycle:
                        </div>
                        <div>
                            14/03/2020 -  14/04/2020
                        </div>
                    </div>
                    <div className="control-item">
                        <div>
                            <Button text="Définir" style="teal" onClick={() => { this.setState({ open: true }) }} />
                        </div>
                    </div>
                </div>
                <div className="modal" style={{ display: `${this.state.open ? "flex" : "none"}` }}>
                    <div className="modal-container">
                        <div className="form-item">
                            <div className="label-container">
                                Prix:
                            </div>
                            <div className="input-container">
                                <InputField />
                            </div>
                        </div>
                        <div className="form-item">
                            <div className="label-container">
                                Début:
                            </div>
                            <div className="input-container">
                                <InputField />
                            </div>
                        </div>
                        <div className="form-item">
                            <div className="label-container">
                                Fin:
                            </div>
                            <div className="input-container">
                                <InputField />
                            </div>
                        </div>
                        <div className="button-container">
                            <Button text="valider" style="teal" onClick={() => { this.setState({ open: false }) }} />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
