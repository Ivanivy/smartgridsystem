import React, { Component } from 'react';
import './Login.css';
import { FacebookIcon, GoogleIcon, TwitterIcon } from '../../images/index';
import { withRouter } from 'react-router-dom';
import {InputField,Button} from '../../components/index';
import { routes } from '../../utils/index';

class Login extends Component {
    render() {
        return (
            <div className="login-page">
                <div className="header-container">
                    <div className="title-container">
                        <div className="title">
                            Login
                        </div>
                        <div className="lead">
                            Enter your credentials to continue
                        </div>
                    </div>
                    <div className="input-container">
                        <div>
                            <div className="label">
                                Username
                            </div>
                            <div className="input-field">
                                <InputField placeholder="Enter your user name" />
                            </div>
                        </div>
                        <div style={{ marginTop: "2.5rem" }}>
                            <div className="label" >
                                Password
                            </div>
                            <div className="input-field">
                                <InputField placeholder="Enter your password" />
                            </div>
                        </div>
                    </div>
                    <div className="button-container">
                        <Button text="SIGN UP" />
                        <Button text="LOGIN" style="dark" onClick={() => { this.props.history.push(routes.mainpage + routes.dailyconsumption) }} />
                    </div>
                </div>
                <div className="social-media-container">
                    <div className="social-link-instruction">
                        Or login with
                    </div>
                    <div className="social-media-links">
                        <div className="google">
                            <GoogleIcon />
                        </div>
                        <div className="facebook">
                            <FacebookIcon />
                        </div>
                        <div className="twitter">
                            <TwitterIcon />
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(Login);