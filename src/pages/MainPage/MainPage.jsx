import React, { Component } from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import './MainPage.css';
import { ElectricalIcon, SettingsIcon, PlugIcon, UserIcon, MenuIcon } from '../../images/index';
import { DailyConsumption, DeviceDetails, Devices, Control, Consumptions, MonthlyConsumption, UserProfile } from '../index';
import { routes } from '../../utils/index';


class MainPage extends Component {
    state = {
        selected: 1,
    }
    render() {
        return (
            <div className="main-page page">
                <div className="title-bar">
                    <div className="menu-icon">
                        <MenuIcon />
                    </div>
                    <div className="menu-title">
                        Consumption - Today
                    </div>
                </div>
                {/* display the differents subpages in the main page */}
                <Switch>
                    <Route path={routes.mainpage + routes.monthlyconsumtion}>
                        <MonthlyConsumption />
                    </Route>
                    <Route path={routes.mainpage + routes.dailyconsumption}>
                        <DailyConsumption />
                    </Route>
                    <Route path={routes.mainpage + routes.control}>
                        <Control />
                    </Route>
                    <Route path={routes.mainpage + routes.devices}>
                        <Devices />
                    </Route>
                    <Route path={routes.mainpage + routes.devicedetails}>
                        <DeviceDetails />
                    </Route>
                    <Route path={routes.mainpage + routes.consumptions}>
                        <Consumptions />
                    </Route>
                    <Route path={routes.mainpage + routes.userprofile}>
                        <UserProfile />
                    </Route>
                </Switch>

                {/* display the differents subpages in the main page */}
                <div className="bottom-menu">
                    <div className={"menu-item " + `${this.state.selected == 1 ? "active" : ""}`} >
                        <ElectricalIcon onClick={() => { this.props.history.push(routes.mainpage + routes.dailyconsumption); this.setState({ selected: 1 }); }} />
                    </div>
                    <div className={"menu-item " + `${this.state.selected == 2 ? "active" : ""}`} >
                        <SettingsIcon onClick={() => { this.props.history.push(routes.mainpage + routes.control); this.setState({ selected: 2 }); }} />
                    </div>
                    <div className={"menu-item " + `${this.state.selected == 3 ? "active" : ""}`}>
                        <PlugIcon onClick={() => { this.props.history.push(routes.mainpage + routes.devices); this.setState({ selected: 3 }); }} />
                    </div>
                    <div className={"menu-item" + `${this.state.selected == 4 ? "active" : ""}`}>
                        <UserIcon onClick={() => { this.props.history.push(routes.mainpage + routes.userprofile); this.setState({ selected: 4 }); }} />
                    </div>
                </div>
            </div>
        )
    }
}
export default withRouter(MainPage);