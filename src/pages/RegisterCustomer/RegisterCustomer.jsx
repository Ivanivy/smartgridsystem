import React, { Component } from 'react';
import './RegisterCustomer.css';
import DefiningProfile from './DefineProfile/DefineProfile';
import ChoosingLocation from './ChooseLocation/ChooseLocation';
import { Button } from '../../components/index';

export default class RegisterCustomer extends Component {
    render() {
        return (
            <div className="register-customer">
                <div className="progress-container">
                    <div className="progress-bar-container">
                        <div className="one ">
                            1
                        </div>
                        <div></div>
                        <div className="two active">
                            2
                        </div>
                    </div>
                    <div className="label">
                        Location
                    </div>
                </div>
                <DefiningProfile />
                <div className="button-container">
                    <Button text="Next" style="teal" />
                </div>
            </div>
        )
    }
}
