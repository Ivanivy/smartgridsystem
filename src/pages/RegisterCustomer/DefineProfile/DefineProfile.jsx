import React, { Component } from 'react';
import { InputField } from '../../../components/index';

export default class DefineProfile extends Component {
    render() {
        return (
            <div className="defining-profile">
                <div className="profile-picture">
                    <div className="user-profile">
                        <div className="user-pic">
                            <img src={require('../../../images/ElectricalNetwork.png')} alt="" srcset="" />
                        </div>
                    </div>
                    <div className="label">
                        Choose picture
                    </div>
                </div>
                <div className="user-form">
                    <div className="form-input-container">
                        <InputField placeholder="Username" />
                        <InputField placeholder="Firt name" />
                        <InputField placeholder="Email" />
                    </div>
                </div>
                
            </div>
        )
    }
}
