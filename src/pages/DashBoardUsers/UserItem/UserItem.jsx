import React, { Component } from 'react';
import { ToogleButton } from '../../../components/index';
import {Edit} from '../../../images/index'
export default class UserItem extends Component {
    render() {
        return (
            <div>
                <div className="user-item-card">
                    <div className="user-short-details">
                        <div className="first">
                            <div className="user-profile">
                                <div className="user-pic">
                                    <img src={require('../../../images/ElectricalNetwork.png')} alt="" srcset="" />
                                </div>
                            </div>
                            <div className="user-id">ID: 112233457</div>
                        </div>
                        <div className="actions">
                            <Edit />
                            <ToogleButton />
                        </div>
                    </div>
                    <div className="user-details">
                        <div className="user-details-item">NAME: TAMKO</div>
                        <div className="user-details-item">SURNAME: CLARENCE KOUADJO</div>
                        <div className="user-details-item">LOCATION BIYEMASSI, YAOUNDE III</div>
                    </div>
                </div>
            </div>
        )
    }
}
