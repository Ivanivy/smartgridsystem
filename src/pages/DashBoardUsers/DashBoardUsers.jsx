import React, { Component } from 'react';
import { Edit } from '../../images/index';
import { ToogleButton } from '../../components/index';
import './DashBoardUsers.css';
import UserItem from './UserItem/UserItem';

export default class DashBoardUsers extends Component {
    render() {
        return (
            <div className="dashboard-users-list">
                <UserItem />
                <UserItem />
                <UserItem />
            </div>
        )
    }
}
