import React from 'react';
import './Consumption.css';


export default function Consumption({ date = "January", price = "0 ", consumption = "0 KW" }) {
    return (
        <div className="card-item consumption-item">
            <div className="left">
                <div className="date">
                    {date}
                </div>
                <div className="price">
                    {price} xaf
                </div>
            </div>
            <div className="right">
                <div className="consumption">
                    {consumption} KW
                </div>
            </div>
        </div>
    )
}
