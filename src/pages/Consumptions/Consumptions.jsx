import React, { Component } from 'react';
import Consumption from './Consumption/Consumption';
import './Consumptions.css';

export default class Consumptions extends Component {
    render() {
        return (
            <div className="page-contain consumptions">
                <Consumption date="March" price="8,000" consumption="10" />
                <Consumption date="April" price="2,000" consumption="11" />
                <Consumption date="April" price="2,000" consumption="11" />
                <Consumption date="April" price="2,000" consumption="11" />
                <Consumption date="April" price="2,000" consumption="11" />
                <Consumption date="April" price="2,000" consumption="11" />
            </div>
        )
    }
}
