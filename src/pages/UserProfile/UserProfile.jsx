import React, { Component } from 'react';
import { CircularProgress, Button } from '../../components/index';
import { MenuIcon, UserIcon } from '../../images/index';
import './UserProfile.css';

export default class UserProfile extends Component {
    render() {
        return (
            <div className="page-contain user-profile">
                <div className="user-profile-header">
                    <div className="title-bar">
                        <div className="menu">
                            <MenuIcon />
                        </div>
                        <div className="menu-title">
                            Consumption - Today
                        </div>
                    </div>

                    <div className="user-icon-container">
                        <UserIcon />
                    </div>
                </div>
                <div className="user-details">
                    <div className="username">
                        Tamko Clarence
                    </div>
                    <div className="user-email">
                        tclarence@gmail.com
                    </div>
                </div>
                <div className="card-item">
                    <div className="circular-graph">
                        <CircularProgress />
                    </div>
                    <div className="consumption-container">
                        <div className="label">
                            Consumption:
                        </div>
                        <div className="consumption-button">
                            <Button style="teal" text="135 watts" />
                        </div>
                        <div className="label">
                            Estimated Bill:
                        </div>
                        <div className="consumption-button">
                            <Button style="teal" text="135 watts" />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
