import React, { Component } from 'react';
import './MainDashBoard.css';
import { InputField } from '../../components/index';
import { TreeDots, Search, AddUser } from '../../images/index';
import { DashBoardUsers, BillingPage, RegisterCustomer } from '../../pages/index'

export default class MainDashBoardPage extends Component {
    render() {
        return (
            <div className="main-dashboard">
                <div className="left-menu">
                    <div className="brand-container">
                        <span className="brand-title">
                            <span>Smart</span><span>Grid</span>
                        </span>
                    </div>
                    <div className="menu">
                        <div className="menu-item">
                            Visualisation Card
                        </div>
                        <div className="menu-item active">
                            Consumers
                        </div>
                    </div>
                    <div className="three-dots">
                        <TreeDots />
                    </div>
                </div>
                <div className="right-side">
                    <div className="search-container">
                        <div className="search-field">
                            <div className="search-input-container">
                                <Search />
                                <InputField placeholder="Search" />
                            </div>
                        </div>
                        <div className="user-actions">
                            <div className="add-user">
                                <AddUser />
                            </div>
                            <div className="user-profile">
                                <div className="user-pic">
                                    <img src={require('../../images/ElectricalNetwork.png')} alt="" srcset="" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="dash-body">
                        {/* <DashBoardUsers/> */}
                        {/* <BillingPage /> */}
                        <RegisterCustomer />
                    </div>
                </div>
            </div>
        )
    }
}
