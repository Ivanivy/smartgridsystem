import React, { Component } from 'react';
import './DeviceDetails.css';
import Device from '../Devices/Device/Device';
import { ToogleButton, ProgressBar } from '../../components/index';
import { LeftArrow, Shutdown } from '../../images/index';

export default class DeviceDetails extends Component {
    render() {
        return (
            <div className="page-contain device-details">
                <div className="device-header">
                    <div className="back-button">
                        <LeftArrow />
                    </div>
                    <div className="device-label">
                        Fridge
                    </div>
                    <div className="device-item">
                        <Device />
                    </div>
                </div>
                <div className="details card-item">
                    <div className="shutdown-container">
                        <div>
                            Eteindre
                        </div>
                        <div>
                            <ToogleButton />
                        </div>
                    </div>
                    <div className="detail-item">
                        <div className="label">
                            Temps de branchement:
                        </div>
                        <div className="value">
                            08:00:01
                        </div>
                    </div>
                    <div className="detail-item">
                        <div className="label">
                            Prix estimé:
                        </div>
                        <div className="value">
                            10 000 FCFA
                        </div>
                    </div>
                    <div className="detail-item">
                        <div className="label">
                            Consomation moyenne:
                        </div>
                        <div >
                            <ProgressBar /> <div><strong>80%</strong> </div>
                        </div>
                    </div>
                    <div className="detail-item">
                        <div className="label">

                        </div>
                        <div className="value">

                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
