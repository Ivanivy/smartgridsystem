import React from 'react';
import './BillItem.css';
import { Pdf } from '../../../images/index';
import { Download } from '../../../images/index';
export default function BillItem() {
    return (
        <div className="bill-card-item">
            <div className="icon">
                <Pdf />
            </div>
            <div className="content">
                <div className="content-1">
                    ID: 1123441342342
                </div>
                <div className="content-2">
                    MARCH 2019
                </div>

            </div>
            <div className="download">
                <Download />
            </div>
        </div>
    )
}
