import React, { Component } from 'react'
import './BillingPage.css';
import { ToogleButton } from '../../components/index';
import { Edit } from '../../images/index';
import BillItem from './BillItem/BillItem';
export default class BillingPage extends Component {
    render() {
        return (
            <div className="billing-page">
                <div className="user-short-details">
                    <div className="first">
                        <div className="user-profile">
                            <div className="user-pic">
                                <img src={require('../../images/ElectricalNetwork.png')} alt="" srcset="" />
                            </div>
                        </div>
                        <div className="user-id">TAMKO CLARENCE</div>
                    </div>
                    <div className="actions">
                        <Edit />
                        <ToogleButton />
                    </div>
                </div>
                <div className="user-details">
                    <div className="user-bills">
                        <BillItem />
                        <BillItem />
                        <BillItem />
                        <BillItem />
                        <BillItem />
                    </div>
                    <div className="date-picker">
                        fdsfsd
                    </div>
                </div>
            </div>
        )
    }
}
