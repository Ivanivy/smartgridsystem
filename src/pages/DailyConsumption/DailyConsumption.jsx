import React, { Component } from 'react';
import { CircularProgress, Button } from '../../components/index';
import './DailyConsumption.css';
import { CanvasJSChart, CanvasJS } from 'canvasjs-react-charts';

export default class DailyConsumption extends Component {
    options = {

        animationEnabled: true,
        theme: "light2", //"light1", "dark1", "dark2"
        axisY: {
            includeZero: true
        },
        dataPointWidth: 20,
        data: [{
            type: "column", //change type to bar, line, area, pie, etc
            //indexLabel: "{y}", //Shows y value on all Data Points
            indexLabelFontColor: "#5A5757",
            indexLabelPlacement: "outside",
            dataPoints: [
                { x: 70, y: 38, color: "#16a085" },
                { x: 80, y: 92, indexLabel: "Highest", color: "#16a085" },
                { x: 90, y: 54, color: "#16a085" },
                { x: 100, y: 60, color: "#16a085" },
            ]
        }]
    }
    render() {
        return (
            <div className="page-contain daily-consumption">
                <div className="card-item">
                    <div className="circular-graph">
                        <CircularProgress />
                    </div>
                    <div className="consumption-container">
                        <div className="label">
                            Consumption:
                    </div>
                        <div className="consumption-button">
                            <Button style="teal" text="135 watts" />
                        </div>
                    </div>
                </div>
                <div className="chart-container" style={{ marginTop: "1.5rem" }}>
                    <CanvasJSChart options={this.options} />
                </div>
                <div className="monthly-button-container">
                    <Button  style="teal" text="View for month"/>
                </div>
            </div>
        )
    }
}
