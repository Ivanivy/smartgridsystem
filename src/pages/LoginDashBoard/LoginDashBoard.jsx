import React, { Component } from 'react'
import './LoginDashBoard.css';
import { InputField, Button } from '../../components/index';

export default class LoginDashboard extends Component {
    render() {
        return (
            <div className="login-dashboard">
                <div className="form-container">
                    <div className="title">
                        <span className="form-title"><span>Smart</span><span>Grid</span></span>
                    </div>
                    <div className="form-inputs">
                        <InputField placeholder="User Name" />
                        <InputField placeholder="Password" />
                    </div>
                    <div className="button">
                        <Button text="Login" style="teal"/>
                    </div>
                </div>
            </div>
        )
    }
}
