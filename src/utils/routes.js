const routes = {
    login: "/login",
    mainpage: "/main",
    dailyconsumption: "/dailyconsumption",
    monthlyconsumtion: "/monthlyconsumption",
    devices: "/devices",
    device: "/device",
    control: "/control",
    devicedetails: "/devicedetails",
    consumptions: "/consumptions",
    userprofile: "/userprofile",
    loginDashboard: "/logindashboard",
    dashboardMainPage: "/main/dashboard",
}


export default routes;