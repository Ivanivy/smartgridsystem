import React, { Component } from 'react';
import { Button as ButtonSem } from 'semantic-ui-react';
import './Button.css';
//diferents styles are 
//light-outlined and primary
const Button = ({ style = "light", text = "Button", onClick }) => {
    return (
        <ButtonSem onClick={onClick} className={`btn btn-${style}`}>{text} </ButtonSem>
    )
}


export default Button;