import React from 'react'
import './CircularProgress.css';
export default function CircularProgress() {
    return (
        <div class="pie-wrapper progress-full">
            <span class="label">
                <div className="value">
                    220
                </div>
                <div className="volts">
                    volts
                </div>
            </span>
            <div class="pie">
                <div class="left-side half-circle"></div>
                <div class="right-side half-circle"></div>
            </div>
        </div>

    )
}
