import React, { useState } from 'react';
import './InputField.css';

const InputField = ({ onChange, placeholder = " ", className, name }) => {
    const [isFocusedState, setIsFocusedState] = useState("")

    const handleFocus = (event) => {
        setIsFocusedState("is-focused")
    }

    const handleBlur = (event) => {
        setIsFocusedState("")
    }
    return (<div class="wrapper">
        <div class="form-group">
            <input type="text" class={`form-control ${isFocusedState}`} placeholder={placeholder} onFocus={handleFocus} onBlur={handleBlur} />
        </div>
    </div>)
}

export default InputField;