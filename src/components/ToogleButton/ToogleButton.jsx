import React from 'react';
import './ToogleButton.css';
import { Shutdown } from '../../images/index';
import { useState } from 'react';
export default function ToogleButton() {
    const [enabled, setEnabled] = useState(false);
    const [translateX, settranslateX] = useState("0");
    const [backgroundInnerCircle, setbackgroundInnerCircle] = useState("#d46666ba")
    return (
        <div className="toogle-button">
            <div className="inner-circle" style={{ transform: `translate(${translateX}%)`, backgroundColor: backgroundInnerCircle }}>
                <Shutdown onClick={() => { setEnabled(!enabled); enabled ? settranslateX(90) : settranslateX(0); enabled ? setbackgroundInnerCircle("#16A085") : setbackgroundInnerCircle("#d46666ba"); }} />
            </div>
        </div>
    )
}
