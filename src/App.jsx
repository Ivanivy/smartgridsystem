import React, { Component } from 'react';
import 'semantic-ui-css/semantic.min.css';
import './app.css';
import { Route, Switch, withRouter } from 'react-router-dom';
/*importing pages*/
import { BrowserRouter } from 'react-router-dom';
import Login from './pages/Login/Login';
import { MainPage, MainDashBoardPage, LoginDashBoard } from './pages/index';
import { routes } from './utils/index';
class App extends Component {
    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route path={routes.loginDashboard}>
                        <LoginDashBoard />
                    </Route>
                    <Route path={routes.dashboardMainPage}>
                        <MainDashBoardPage />
                    </Route>
                    <Route path={routes.login}>
                        <Login />
                    </Route>
                    <Route path={routes.mainpage}>
                        <MainPage />
                    </Route>
                </Switch>
            </BrowserRouter >
        );
    }
}

export default withRouter(App);
